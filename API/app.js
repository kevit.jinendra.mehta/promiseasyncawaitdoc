const express = require('express');
const app = express();

app.get('/:milis',(req, res) => {
    setTimeout(() => {
        res.status(200).send('Response After ' + req.params.milis +' ms');
    }, parseInt(req.params.milis));
});

app.listen(3000, () => {
    console.log('server listening on 3000');
});