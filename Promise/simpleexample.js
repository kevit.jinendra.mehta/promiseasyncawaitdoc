const f1 = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve({
                data: 'data'
            });
        }, 5000);
    });
};

f1().then((result) =>{
    console.log(result);
}).catch((e) => {
    console.log(e);
});