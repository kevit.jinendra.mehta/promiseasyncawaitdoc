
const F = (name) => {
    return new Promise((resolve, reject) => {
        console.log(name + ' started')
        setTimeout(() => {
            console.log(name + ' completed');
            resolve();
        },1 + Math.random()*1000);
    });
};

F('start').then(() => {
    let promiseA = F('A');
    let promiseB = promiseA.then(() => {
        return F('B');  // assigning proper promise by returning it is important
    });

    let promiseD = F('D');

    let promiseAD = Promise.all([promiseA, promiseD]);

    let promiseC = promiseAD.then( () => {
        return F('C'); // assign promise to promiseC
    });

    let promiseExit =  Promise.all([promiseB, promiseC]);

    return promiseExit;    // return correct pending promise
    // if we return wrong value(or not return anything) then following then() may execute directly
    // and not behave as expected

}).then(() => {  // callback passed in this then() is executed when promise returned in above then() is fulfilled

    console.log('EXIT');
}).catch((e) => {
    console.log(e);
});
