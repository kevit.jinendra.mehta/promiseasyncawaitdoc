let p1 =  new Promise(resolve => {
    console.log('promise1 pending');
    console.log('------------------------');
    setTimeout(() => {
        console.log('promise1 fulfilled');
        console.log('------------------------');
        resolve(123);
    }, 500 + Math.random()*1000);
});

const f2 =  function() {
    return new Promise(resolve => {
        console.log('promise2 pending');
        console.log('------------------------');
        setTimeout(() => {
            console.log('promise2 fulfilled');
            console.log('------------------------');
            resolve({
                data: 'data'
            });
        }, 1000 + Math.random()*1000);
    })
};


let p2 = f2();

console.log('p1:  ',p1);
console.log('------------------------');
console.log('p2:  ',p2);
console.log('------------------------');


Promise.all([p1, p2]).then((values) => {
    console.log('ALL GIVEN PROMISES FULFILLED');
    console.log('VALUES PASSED IN resolve():');
    console.log(values);
});