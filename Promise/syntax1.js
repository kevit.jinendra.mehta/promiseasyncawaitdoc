const axios = require('axios');

//calling a function that returns a promise
//|_____________________________________|
axios.get('http://localhost:3000/1000').then((response) => {
    console.log(response.data);
    return Promise.reject('error');  //this promise rejection is not handled
}, (e) => {
    console.log(e);
});