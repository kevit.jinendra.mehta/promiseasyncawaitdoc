const axios = require('axios');

axios.get('http://localhost:3000/1000').then((response) => {
    console.log(response.data);
    let condition = true;
    if(condition) {
        return Promise.reject('error');
    }
    return axios.get('http://localhost:3000/500');
}).then((response) => {
    console.log(response.data);
    return axios.get('http://localhost:3000/750');
}).then((response) => {
    console.log(response.data);
}).catch((e) => {
    console.log(e);  // this code will get executed
});