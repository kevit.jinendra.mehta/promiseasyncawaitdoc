const axios = require('axios');

const f1 = async () => {
    await axios.get('http://localhost:3000/5000');
};

const f2 = async () => {
    await axios.get('http://localhost:3000/1000');
};

const f3 = async () => {
    await Promise.all([f1(), f2()]);
    console.log('Waited for two async functions');
};

f3();